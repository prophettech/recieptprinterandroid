package com.prophettech.printerhelper.wordpress.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.printerhelper.R;
import com.prophettech.printerhelper.activity.BaseActivity;
import com.prophettech.printerhelper.utils.SunmiPrintHelper;
import com.prophettech.printerhelper.wordpress.adapter.ItemsAdapter;
import com.prophettech.printerhelper.wordpress.models.OrderStatus;
import com.prophettech.printerhelper.wordpress.models.OrdersModel;
import com.prophettech.printerhelper.wordpress.models.RefundModel;
import com.prophettech.printerhelper.wordpress.models.RefundResponse;
import com.prophettech.printerhelper.wordpress.rest.APIInterface;
import com.prophettech.printerhelper.wordpress.rest.RetrofitClientInstance;
import com.prophettech.printerhelper.wordpress.rest.Services;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailActivity extends BaseActivity {

    @BindView(R.id.tvAddress)
    TextView mAddress;
    @BindView(R.id.tvName)
    TextView mName;
    @BindView(R.id.tvOrderNumber)
    TextView mOrderNumber;
    @BindView(R.id.tvStatus)
    TextView mStatus;
    @BindView(R.id.tvPrice)
    TextView mPrice;
    @BindView(R.id.tvShippingMethod)
    TextView mShippingMethod;
    @BindView(R.id.tvCustomerNote)
    TextView mCustomerNote;
    @BindView(R.id.btPrint)
    Button mPrintButton;
    @BindView(R.id.btCompleteOrder)
    Button mCompleteOrder;
    @BindView(R.id.tvPaymentMethod)
    TextView mPaymentMethod;
    @BindView(R.id.rvItems)
    RecyclerView mRecyclerView;
    private String mOrderId;
    private ItemsAdapter mAdapter;
    private OrdersModel mOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ItemsAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mOrderId = getIntent().getStringExtra("ORDER_ID");
        getOrderDetails(mOrderId);
    }

    private void getOrderDetails(String orderId) {
        showProgress(true);

        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<OrdersModel> call = service.getOrder(orderId, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<OrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<OrdersModel> call, @NonNull Response<OrdersModel> response) {
                showProgress(false);
                OrdersModel body = response.body();
                if (body != null) {
                    mOrder = body;
                    setData(body);
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrdersModel> call, @NonNull Throwable t) {
                showProgress(false);
            }
        });
    }

    private void setData(OrdersModel body) {
        mName.setText(body.getBilling().getFirstName() + " " + body.getBilling().getLastName());
        mOrderNumber.setText(String.format("#%s", body.getNumber()));
        mPrice.setText(String.format("£%s", body.getTotal()));
        mShippingMethod.setText(body.getShippingLines().get(0).getMethodTitle());
        mPaymentMethod.setText(body.getPaymentMethodTitle());
        mCustomerNote.setText(body.getCustomerNote());
        mAdapter.refresh(body.getLineItems());

        StringBuilder address = new StringBuilder();
        address.append(body.getBilling().getAddress1());
        address.append("\n");
        address.append(body.getBilling().getAddress2());
        address.append("\n");
        address.append(body.getBilling().getPostcode());
        address.append("\n");
        address.append(body.getBilling().getPhone());
        address.append("\n");
        address.append(body.getBilling().getEmail());
        mAddress.setText(address);

        switch (body.getStatus()) {
            case "refunded":
                mStatus.setText(body.getStatus());
            case "cancelled":
                mStatus.setText(body.getStatus());
                mStatus.setTextColor(Color.RED);
                break;
            case "processing":
                mStatus.setText(body.getStatus());
                mStatus.setTextColor(Color.BLUE);
                break;
            case "completed":
                mStatus.setText(body.getStatus());
                mStatus.setTextColor(Color.parseColor("#ff669900"));
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.btCompleteOrder)
    public void completeOrder() {
        updateOrderStatus("completed");
    }

    @OnClick(R.id.btRefund)
    public void refundOrder() {
        showProgress(true);
        RefundModel refundModel = new RefundModel();
        refundModel.setAmount(mOrder.getTotal());
        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<RefundResponse> call = service.refundOrder(mOrderId, refundModel, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<RefundResponse>() {
            @Override
            public void onResponse(@NonNull Call<RefundResponse> call, @NonNull Response<RefundResponse> response) {
                Toast.makeText(OrderDetailActivity.this, "Refund Successful!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(@NonNull Call<RefundResponse> call, @NonNull Throwable t) {
                showProgress(false);
                showToastMessage("Error updating order!");
            }
        });
        updateOrderStatus("refunded");
    }

    @OnClick(R.id.btPrint)
    public void printOrder() {
        SunmiPrintHelper.printOrder(mOrder);
    }

    private void updateOrderStatus(String status) {
        showProgress(true);
        OrderStatus orderStatus = new OrderStatus();
        orderStatus.setStatus(status);
        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<OrdersModel> call = service.updateOrder(mOrderId, orderStatus, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<OrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<OrdersModel> call, @NonNull Response<OrdersModel> response) {
                showProgress(false);
                showToastMessage("Order " + status + "!");
                finish();
            }

            @Override
            public void onFailure(@NonNull Call<OrdersModel> call, @NonNull Throwable t) {
                showProgress(false);
                showToastMessage("Error updating order!");
            }
        });
    }
}