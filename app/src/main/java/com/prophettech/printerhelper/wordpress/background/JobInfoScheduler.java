package com.prophettech.printerhelper.wordpress.background;

import android.content.Context;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;

import java.util.concurrent.TimeUnit;

public class JobInfoScheduler {
    private static String TAG = JobInfoScheduler.class.getSimpleName();
    private static JobInfoScheduler instance = null;
    final int periodicity = (int) TimeUnit.MINUTES.toSeconds(1); // Every 12 min periodicity expressed as seconds
    final int toleranceInterval = (int) TimeUnit.MINUTES.toSeconds(1); // a small(ish) window of time when triggering is OK
    private Context mContext;


    private JobInfoScheduler() {
    }

    public static JobInfoScheduler getInstance() {
        if (instance == null) {
            instance = new JobInfoScheduler();
        }
        return instance;
    }

    public void scheduleJob(String jobTag, Context context) {
        FirebaseJobDispatcher jobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        Job syncJob = jobDispatcher.newJobBuilder()
                .setService(BackgroundOrderService.class)
                .setLifetime(Lifetime.FOREVER)
                .setReplaceCurrent(true)
                .setTag(jobTag)
                .setTrigger(Trigger.executionWindow(periodicity, toleranceInterval + periodicity))
                .setRecurring(true)
                //.setConstraints(Constraint.ON_ANY_NETWORK)
                .build();
        jobDispatcher.mustSchedule(syncJob);
        Log.d(TAG, "Job Scheduled ");
    }
}
