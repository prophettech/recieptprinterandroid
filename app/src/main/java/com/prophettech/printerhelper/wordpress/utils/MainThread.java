package com.prophettech.printerhelper.wordpress.utils;

import android.os.Handler;
import android.os.Looper;

public class MainThread {

    private static MainThread instance = null;
    private Handler handler;

    private MainThread() {
        handler = new Handler(Looper.getMainLooper());
    }

    public static MainThread getInstance() {
        if (instance == null) {
            instance = new MainThread();
        }
        return instance;
    }

    public void post(Runnable runnable) {
        handler.post(runnable);
    }

    public void postDelayed(Runnable runnable, long i) {
        handler.postDelayed(runnable, i);
    }
}