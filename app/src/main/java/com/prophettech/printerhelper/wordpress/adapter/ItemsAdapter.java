package com.prophettech.printerhelper.wordpress.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.printerhelper.R;
import com.prophettech.printerhelper.wordpress.models.LineItem;
import com.prophettech.printerhelper.wordpress.models.MetaDatum;
import com.prophettech.printerhelper.wordpress.rest.Services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder> {

    private List<LineItem> mItems;

    public ItemsAdapter() {
        mItems = new ArrayList<>();
    }

    public void refresh(List<LineItem> items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_order_items, parent, false);
        return new ItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsViewHolder holder, int position) {
        LineItem item = mItems.get(position);
        holder.mQuantity.setText(String.valueOf(item.getQuantity()));
        StringBuilder itemsWithOption = new StringBuilder();
        itemsWithOption.append(item.getName());
        for (MetaDatum data : item.getMetaData()) {
            if (data != null && data.getKey()!= null && Arrays.asList(Services.optionKeys).contains(data.getKey().toLowerCase())) {
                itemsWithOption.append("\n").append("-").append(data.getKey());
                itemsWithOption.append(": ").append(data.getValue());
            }
        }
        holder.mItemName.setText(itemsWithOption);
        holder.mPrice.setText(item.getTotal());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ItemsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvQuantity)
        TextView mQuantity;
        @BindView(R.id.tvItemName)
        TextView mItemName;
        @BindView(R.id.tvPrice)
        TextView mPrice;

        public ItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
