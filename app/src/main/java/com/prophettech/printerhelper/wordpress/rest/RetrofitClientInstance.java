package com.prophettech.printerhelper.wordpress.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prophettech.printerhelper.wordpress.models.MetaDatum;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private static final String BASE_URL = "https://janichicken.co.uk/";
    private static Retrofit retrofit;
    private static RetrofitClientInstance myClient;

    private RetrofitClientInstance() {
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static synchronized RetrofitClientInstance getInstance() {
        if (myClient == null) {
            myClient = new RetrofitClientInstance();
        }
        return myClient;
    }

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .registerTypeAdapter(MetaDatum.class, new MetaDataTypeAdapterFactory())
                    .create();

            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(new OkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public APIInterface getMyApi() {
        return retrofit.create(APIInterface.class);
    }
}
