package com.prophettech.printerhelper.wordpress.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefundResponse {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("date_created_gmt")
    @Expose
    private String dateCreatedGmt;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("refunded_by")
    @Expose
    private Integer refundedBy;
    @SerializedName("refunded_payment")
    @Expose
    private Boolean refundedPayment;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateCreatedGmt() {
        return dateCreatedGmt;
    }

    public void setDateCreatedGmt(String dateCreatedGmt) {
        this.dateCreatedGmt = dateCreatedGmt;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getRefundedBy() {
        return refundedBy;
    }

    public void setRefundedBy(Integer refundedBy) {
        this.refundedBy = refundedBy;
    }

    public Boolean getRefundedPayment() {
        return refundedPayment;
    }

    public void setRefundedPayment(Boolean refundedPayment) {
        this.refundedPayment = refundedPayment;
    }
}
