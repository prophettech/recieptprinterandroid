package com.prophettech.printerhelper.wordpress.background;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.prophettech.printerhelper.utils.SunmiPrintHelper;
import com.prophettech.printerhelper.wordpress.models.OrderStatus;
import com.prophettech.printerhelper.wordpress.models.OrdersModel;
import com.prophettech.printerhelper.wordpress.rest.APIInterface;
import com.prophettech.printerhelper.wordpress.rest.RetrofitClientInstance;
import com.prophettech.printerhelper.wordpress.rest.Services;
import com.prophettech.printerhelper.wordpress.utils.CommonUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrintOrderWork extends Worker {

    private static final String TAG = PrintOrderWork.class.getSimpleName();

    public PrintOrderWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.d(TAG, "Job Started ");
        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<List<OrdersModel>> call = service.getAllOrders("10", Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<List<OrdersModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<OrdersModel>> call, @NonNull Response<List<OrdersModel>> response) {
                if (response.body() != null) {
                    for (OrdersModel orders : response.body()) {
                        Map<String, Long> dateTime = checkTimeDifference(orders.getDateCreated());
                        if (dateTime != null && dateTime.size() > 0 && dateTime.get("days") == 0 && dateTime.get("hours") == 0 && dateTime.get("minutes") < 5 && orders.getStatus().equals("processing")) {
                            SunmiPrintHelper.printOrder(orders);
                            updateOrderStatus(orders.getNumber());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<OrdersModel>> call, @NonNull Throwable t) {
                Log.d(TAG, getClass().getSimpleName() + "Network Error!");
            }
        });
        return Result.success();
    }

    private void updateOrderStatus(String orderId) {
        OrderStatus orderStatus = new OrderStatus();
        orderStatus.setStatus("completed");
        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<OrdersModel> call = service.updateOrder(orderId, orderStatus, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<OrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<OrdersModel> call, @NonNull Response<OrdersModel> response) {
                Log.d(TAG, getClass().getSimpleName() + "Updated Status");
            }

            @Override
            public void onFailure(@NonNull Call<OrdersModel> call, @NonNull Throwable t) {
                Log.d(TAG, getClass().getSimpleName() + "Network Error!");
            }
        });
    }

    private Map<String, Long> checkTimeDifference(String dateCreated) {
        CommonUtils commonUtils = new CommonUtils();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK);
        try {
            Date date1 = simpleDateFormat.parse(dateCreated);
            Date date2 = new Date();
            if (date1 != null) {
                return commonUtils.printDifference(date1, date2);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
