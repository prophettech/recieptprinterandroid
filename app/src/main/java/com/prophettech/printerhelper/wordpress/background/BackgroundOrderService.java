package com.prophettech.printerhelper.wordpress.background;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.prophettech.printerhelper.R;
import com.prophettech.printerhelper.utils.SunmiPrintHelper;
import com.prophettech.printerhelper.wordpress.activity.GetOrdersActivity;
import com.prophettech.printerhelper.wordpress.models.OrderStatus;
import com.prophettech.printerhelper.wordpress.models.OrdersModel;
import com.prophettech.printerhelper.wordpress.rest.APIInterface;
import com.prophettech.printerhelper.wordpress.rest.RetrofitClientInstance;
import com.prophettech.printerhelper.wordpress.rest.Services;
import com.prophettech.printerhelper.wordpress.utils.CommonUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BackgroundOrderService extends JobService {


    private static final String TAG = BackgroundOrderService.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
    }

    @Override
    public boolean onStartJob(JobParameters job) {
        Log.d(TAG, "Job Started ");
        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<List<OrdersModel>> call = service.getAllOrders("10", Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<List<OrdersModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<OrdersModel>> call, @NonNull Response<List<OrdersModel>> response) {
                if (response.body() != null) {
                    for (OrdersModel orders : response.body()) {
                        Map<String, Long> dateTime = checkTimeDifference(orders.getDateCreated());
                        if (dateTime != null && dateTime.size() > 0 && dateTime.get("days") == 0 && dateTime.get("hours") == 0 && dateTime.get("minutes") < 5 && orders.getStatus().equals("processing")) {
                            SunmiPrintHelper.printOrder(orders);
                            updateOrderStatus(orders.getNumber());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<OrdersModel>> call, @NonNull Throwable t) {
                Log.d(TAG, getClass().getSimpleName() + "Network Error!");
            }
        });
        return true;
    }

    public void sendNotification(String title) {
        Intent intent = new Intent(getApplicationContext(), GetOrdersActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT);
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
        }

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setContentTitle(title)
                .setContentText("New Order!")
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(defaultSoundUri)
                .setAutoCancel(true);

        Objects.requireNonNull(notificationManager).notify(0, notification.build());
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        Log.d(TAG, "Job ended");
        return true;
    }

    private void updateOrderStatus(String orderId) {
        OrderStatus orderStatus = new OrderStatus();
        orderStatus.setStatus("completed");
        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<OrdersModel> call = service.updateOrder(orderId, orderStatus, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<OrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<OrdersModel> call, @NonNull Response<OrdersModel> response) {
                Log.d(TAG, getClass().getSimpleName() + "Updated Status");
            }

            @Override
            public void onFailure(@NonNull Call<OrdersModel> call, @NonNull Throwable t) {
                Log.d(TAG, getClass().getSimpleName() + "Network Error!");
            }
        });
    }

    private Map<String, Long> checkTimeDifference(String dateCreated) {
        CommonUtils commonUtils = new CommonUtils();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK);
        try {
            Date date1 = simpleDateFormat.parse(dateCreated);
            Date date2 = new Date();
            if (date1 != null) {
                return commonUtils.printDifference(date1, date2);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
