package com.prophettech.printerhelper.wordpress.rest;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.prophettech.printerhelper.wordpress.models.MetaDatum;

import java.lang.reflect.Type;

public class MetaDataTypeAdapterFactory implements JsonDeserializer<MetaDatum> {

    public static final String TAG = MetaDataTypeAdapterFactory.class.getSimpleName();

    @Override
    public MetaDatum deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        MetaDatum response = new MetaDatum();
        JsonObject object = json.getAsJsonObject();
        if (object.get("value").isJsonArray()) {

        } else if (object.get("value").isJsonObject()) {
            Gson gson = new Gson();
            try {
                String dtoObject = gson.fromJson(object.get("value"), String.class);
                response.setValue(dtoObject);
            } catch (JsonSyntaxException e) {
                Log.e(TAG, "Error " + e);
            }
        } else if (object.get("value").isJsonNull()) {

        } else {
            response.setKey(object.get("key").getAsString());
            response.setValue(object.get("value").getAsString());
        }
        return response;
    }
}
