package com.prophettech.printerhelper.wordpress.rest;

import com.prophettech.printerhelper.wordpress.models.OrderStatus;
import com.prophettech.printerhelper.wordpress.models.OrdersModel;
import com.prophettech.printerhelper.wordpress.models.RefundModel;
import com.prophettech.printerhelper.wordpress.models.RefundResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("wp-json/wc/v3/orders")
    Call<List<OrdersModel>> getAllOrders(@Query("per_page") String perPage, @Query("consumer_key") String consumerKey, @Query("consumer_secret") String consumerSecret);

    @GET("wp-json/wc/v3/orders/{id}")
    Call<OrdersModel> getOrder(@Path("id") String orderId, @Query("consumer_key") String consumerKey, @Query("consumer_secret") String consumerSecret);

    @POST("wp-json/wc/v3/orders/{id}")
    Call<OrdersModel> updateOrder(@Path("id") String orderId, @Body OrderStatus body, @Query("consumer_key") String consumerKey, @Query("consumer_secret") String consumerSecret);

    @POST("wp-json/wc/v3/orders/{id}/refunds")
    Call<RefundResponse> refundOrder(@Path("id") String orderId, @Body RefundModel body, @Query("consumer_key") String consumerKey, @Query("consumer_secret") String consumerSecret);

}
