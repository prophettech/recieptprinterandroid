package com.prophettech.printerhelper.wordpress.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.prophettech.printerhelper.R;
import com.prophettech.printerhelper.activity.BaseActivity;
import com.prophettech.printerhelper.utils.SunmiPrintHelper;
import com.prophettech.printerhelper.wordpress.adapter.OrdersAdapter;
import com.prophettech.printerhelper.wordpress.models.OrderStatus;
import com.prophettech.printerhelper.wordpress.models.OrdersModel;
import com.prophettech.printerhelper.wordpress.rest.APIInterface;
import com.prophettech.printerhelper.wordpress.rest.RetrofitClientInstance;
import com.prophettech.printerhelper.wordpress.rest.Services;
import com.prophettech.printerhelper.wordpress.utils.CommonUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetOrdersActivity extends BaseActivity implements OrdersAdapter.OrderClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private OrdersAdapter mAdapter;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_orders);
        ButterKnife.bind(this);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        initViews();
        getOrders();
    }

    private void initViews() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new OrdersAdapter();
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupSharedPreferences();
    }

    private void getOrders() {
        showProgress(true);

        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<List<OrdersModel>> call = service.getAllOrders("100", Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<List<OrdersModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<OrdersModel>> call, @NonNull Response<List<OrdersModel>> response) {
                if (response.body() != null) {
                    for (OrdersModel order : response.body()) {
                        Map<String, Long> dateTime = checkTimeDifference(order.getDateCreated());
                        if (dateTime != null && dateTime.size() > 0 && dateTime.get("days") == 0 && dateTime.get("hours") == 0 && dateTime.get("minutes") < 5 && order.getStatus().equals("processing")) {
                            getOrderDetails(order.getNumber());
                        }
                    }
                    mAdapter.refresh(response.body(), GetOrdersActivity.this);
                }
                showProgress(false);
            }

            @Override
            public void onFailure(@NonNull Call<List<OrdersModel>> call, @NonNull Throwable t) {
                showProgress(false);
                showToastMessage("Error!");
            }
        });
    }

    private void getOrderDetails(String orderId) {
        showProgress(true);

        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<OrdersModel> call = service.getOrder(orderId, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<OrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<OrdersModel> call, @NonNull Response<OrdersModel> response) {
                showProgress(false);
                OrdersModel body = response.body();
                if (body != null) {
                    SunmiPrintHelper.printOrder(body);
                    updateOrderStatus(body.getNumber());
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrdersModel> call, @NonNull Throwable t) {
                showProgress(false);
            }
        });
    }

    @Override
    public void onOrderClick(String orderId) {
        Intent intent = new Intent(this, OrderDetailActivity.class);
        intent.putExtra("ORDER_ID", orderId);
        startActivity(intent);
    }

    @Override
    public void onPrintClick(OrdersModel order) {
        SunmiPrintHelper.printOrder(order);
    }

    @OnClick(R.id.btRefresh)
    public void refreshOrders() {
        getOrders();
    }

    private Map<String, Long> checkTimeDifference(String dateCreated) {
        CommonUtils commonUtils = new CommonUtils();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK);
        try {
            Date date1 = simpleDateFormat.parse(dateCreated);
            Date date2 = new Date();
            if (date1 != null) {
                return commonUtils.printDifference(date1, date2);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void updateOrderStatus(String orderId) {
        showProgress(true);
        OrderStatus orderStatus = new OrderStatus();
        orderStatus.setStatus("completed");
        APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
        Call<OrdersModel> call = service.updateOrder(orderId, orderStatus, Services.CONSUMER_KEY, Services.CONSUMER_SECRET);
        call.enqueue(new Callback<OrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<OrdersModel> call, @NonNull Response<OrdersModel> response) {
                showProgress(false);
            }

            @Override
            public void onFailure(@NonNull Call<OrdersModel> call, @NonNull Throwable t) {
                showProgress(false);
                showToastMessage("Error updating orders!");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupSharedPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("push")) {
            subscribeToFirebaseTopic();
        }
    }

    private void subscribeToFirebaseTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic("Push_Notifications");
    }
}