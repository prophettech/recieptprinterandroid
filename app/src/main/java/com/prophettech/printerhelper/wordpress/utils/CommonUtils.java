package com.prophettech.printerhelper.wordpress.utils;

import android.util.Log;

import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CommonUtils {


    public static int createRandomCode(int codeLength) {
        char[] chars = "1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new SecureRandom();
        for (int i = 0; i < codeLength; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return Integer.parseInt(sb.toString());
    }


    public static String getTwoDecimal(double val) {
        DecimalFormat df = new DecimalFormat("#######0.00");
        df.setMinimumFractionDigits(2);
        String inter = df.format(val);
        //double output =  Double.valueOf(inter).doubleValue();
        return inter;
    }

    //1 minute = 60 seconds
    //1 hour = 60 x 60 = 3600
    //1 day = 3600 x 24 = 86400
    public Map<String, Long> printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        Log.d("startDate : ", startDate.toString());
        Log.d("endDate : ", endDate.toString());
        Log.d("different : ", String.valueOf(different));

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        HashMap<String, Long> dateTime = new HashMap<String, Long>();
        dateTime.put("days", elapsedDays);
        dateTime.put("hours", elapsedHours);
        dateTime.put("minutes", elapsedMinutes);
        Log.d("-------", "days: " + elapsedDays + "\nhours: " + elapsedHours + "\nminutes: " + elapsedMinutes + "\nseconds" + elapsedSeconds);

        return dateTime;
    }
}
