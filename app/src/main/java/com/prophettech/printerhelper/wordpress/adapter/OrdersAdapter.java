package com.prophettech.printerhelper.wordpress.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.prophettech.printerhelper.R;
import com.prophettech.printerhelper.wordpress.models.OrdersModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrdersViewHolder> {

    private List<OrdersModel> mOrders;
    private OrderClickListener mOrderClickListener;

    public OrdersAdapter() {
        mOrders = new ArrayList<>();
    }

    public void refresh(List<OrdersModel> orders, OrderClickListener clickListener) {
        this.mOrders = orders;
        this.mOrderClickListener = clickListener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_orders_item, parent, false);
        return new OrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersViewHolder holder, int position) {
        OrdersModel item = mOrders.get(position);
        holder.mOrderNumber.setText(String.format("#%s", item.getNumber()));
        holder.mPrice.setText(String.format("£%s", item.getTotal()));
        switch (item.getStatus()) {
            case "refunded":
                holder.mStatus.setText(item.getStatus());
            case "cancelled":
                holder.mStatus.setText(item.getStatus());
                holder.mStatus.setTextColor(Color.RED);
                break;
            case "processing":
                holder.mStatus.setText(item.getStatus());
                holder.mStatus.setTextColor(Color.BLUE);
                break;
            case "completed":
                holder.mStatus.setText(item.getStatus());
                holder.mStatus.setTextColor(Color.parseColor("#ff669900"));
                break;
            default:
                break;
        }
        holder.mShippingMethod.setText(item.getShippingLines().get(0).getMethodTitle());
        holder.mName.setText(item.getBilling().getFirstName() + " " + item.getBilling().getLastName());
        holder.mAddress.setText(item.getBilling().getAddress1());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK);
        Date date = new Date();
        try {
            date = format.parse(item.getDateCreated());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        format = new SimpleDateFormat("MM/dd/yy'\n'hh:mm a", Locale.getDefault());
        String dateAndTime = null;
        if (date != null) {
            dateAndTime = format.format(date);
        }
        holder.mDate.setText(dateAndTime);
        holder.mCardView.setOnClickListener(v -> mOrderClickListener.onOrderClick(item.getNumber()));
        holder.mPrintButton.setOnClickListener(v -> mOrderClickListener.onPrintClick(item));
    }

    @Override
    public int getItemCount() {
        return mOrders.size();
    }

    public interface OrderClickListener {
        void onOrderClick(String orderId);

        void onPrintClick(OrdersModel order);
    }

    public static class OrdersViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvAddress)
        TextView mAddress;
        @BindView(R.id.tvName)
        TextView mName;
        @BindView(R.id.tvOrderNumber)
        TextView mOrderNumber;
        @BindView(R.id.tvStatus)
        TextView mStatus;
        @BindView(R.id.tvPrice)
        TextView mPrice;
        @BindView(R.id.tvShippingMethod)
        TextView mShippingMethod;
        @BindView(R.id.tvDate)
        TextView mDate;
        @BindView(R.id.btPrint)
        Button mPrintButton;
        @BindView(R.id.mCardView)
        CardView mCardView;

        public OrdersViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
